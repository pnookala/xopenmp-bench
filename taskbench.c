#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <omp.h>

#define DEFAULT_DELAY_TIME 0.10  // Default delaytime in microseconds
#define DEFAULT_NUM_REPS 20

int nthreads = -1;           // Number of OpenMP threads
int delaylength = -1;        // The number of iterations to delay for
int outerreps = -1;          // Outer repetitions
double delaytime = -1.0;     // Length of time to delay for in microseconds
unsigned long innerreps = 8388608; // Inner repetitions
double *times;           // Array of doubles storing the benchmark times in microseconds
double referencetime;    // The average reference time in microseconds to perform
// outerreps runs
double referencesd;      // The standard deviation in the reference time in
// microseconds for outerreps runs.
double testtime;         // The average test time in microseconds for
// outerreps runs
double testsd;       // The standard deviation in the test time in
// microseconds for outerreps runs.

void usage() {
  printf("Usage: ./build/taskbench \n"
	 "\t--num-reps <num-reps> (default %d)\n"
	 "\t--delay-time <delay-time> (default %0.4f microseconds)\n",
	 DEFAULT_NUM_REPS, DEFAULT_DELAY_TIME );
}

double getclock() {
  struct timespec nowtime;
  clock_gettime(CLOCK_MONOTONIC, &nowtime);
  return nowtime.tv_sec + 1.0e-9 * nowtime.tv_nsec;
}

void parse_arguments(int argc, char *argv[]) {
  int arg;
  for (arg = 1; arg < argc; arg++) {
    if (strcmp(argv[arg], "--num-reps") == 0) {
      outerreps = atoi(argv[++arg]);
      if (outerreps == 0) {
        printf("Invalid integer:--num-reps: %s\n", argv[arg]);
        usage();
        exit(EXIT_FAILURE);
      }
    }
    else if (strcmp(argv[arg], "--delay-time") == 0.0) {
      delaytime = atof(argv[++arg]);
      if (delaytime == 0.0) {
        printf("Invalid float:--delay-time: %s\n", argv[arg]);
        usage();
        exit(EXIT_FAILURE);
      }
    }
    else if (strcmp(argv[arg], "-h") == 0) {
      usage();
      exit(EXIT_SUCCESS);
    }
    else {
      printf("Invalid parameters: %s\n", argv[arg]);
      usage();
      exit(EXIT_FAILURE);
    }
  }
}

void delay(int delaylength) {

  int i;
  float a = 0.;

  for (i = 0; i < delaylength; i++)
    a += i;
  if (a < 0)
    printf("%f \n", a);

}

int getdelaylengthfromtime(double delaytime) {
  int i, reps;
  double lapsedtime, starttime; // seconds

  reps = 1000;
  lapsedtime = 0.0;

  delaytime = delaytime/1.0E6; // convert from microseconds to seconds

  // Here we want to use the delaytime in microseconds to find the 
  // delaylength in iterations. We start with delaylength=0 and 
  // increase until we get a large enough delaytime, return delaylength 
  // in iterations. 

  delaylength = 0;
  delay(delaylength);

  while (lapsedtime < delaytime) {
    delaylength = delaylength * 1.1 + 1;
    starttime = getclock();
    for (i = 0; i < reps; i++) {
      delay(delaylength);
    }
    lapsedtime = (getclock() - starttime) / (double) reps;
  }
  return delaylength;
}

void init(int argc, char **argv)
{
  //Create a parallel region
#pragma omp parallel
  {
#pragma omp master
    {
      nthreads = omp_get_num_threads();
    }
  }

  parse_arguments(argc, argv);

  if (outerreps == -1) {
    outerreps = DEFAULT_NUM_REPS;
  }

  if (delaytime == -1.0) {
    delaytime = DEFAULT_DELAY_TIME; 
  }

  delaylength = getdelaylengthfromtime(delaytime); // Always need to compute delaylength in iterations 
    
  times = malloc((outerreps+1) * sizeof(double));

  printf("Running X-OpenMP benchmark\n"
	 "\t%d thread(s)\n"
	 "\t%d outer repetitions\n"
   "\t%lu inner repetitions\n"
	 "\t%d delay length (iterations) \n"
	 "\t%f delay time (microseconds)\n",
	 nthreads, outerreps, innerreps,
	 delaylength, delaytime);
}

void benchmark(char *name, void (*test)(void))
{
  double start;
  //Repeat the experiment 10 times and calculates the average
  for (int k=0; k<=outerreps; k++) {
    start = getclock();
    test();
    times[k] = (getclock() - start) * 1.0e6 / (double) innerreps;
  }
}

/* Test parallel task generation overhead */
void getTaskOverhead() {
#pragma omp parallel
  {
    for (int j = 0; j < innerreps; j ++ ) {
#pragma omp task
      {
	delay( delaylength );

      } // task
    }; // for j
  } // parallel

}

void print_stats() {
  double meantime, totaltime, sumsq, mintime, maxtime, sd;
  int i;

  mintime = 1.0e10;
  maxtime = 0.;
  totaltime = 0.;

  for (i = 1; i <= outerreps; i++) {
    mintime = (mintime < times[i]) ? mintime : times[i];
    maxtime = (maxtime > times[i]) ? maxtime : times[i];
    totaltime += times[i];
  }

  meantime = totaltime / outerreps;
  sumsq = 0;

  for (i = 1; i <= outerreps; i++) {
    sumsq += (times[i] - meantime) * (times[i] - meantime);
  }
  sd = sqrt(sumsq / (outerreps - 1));

  printf("\n");
  printf("Sample_size       Average     Min         Max          S.D.          \n");
  printf(" %d                %f   %f   %f    %f      \n",
	 outerreps, meantime, mintime, maxtime, sd);
  printf("\n");
}

int main(int argc, char **argv) {
  init(argc, argv);
  benchmark("TASK", &getTaskOverhead);
  print_stats();
}
