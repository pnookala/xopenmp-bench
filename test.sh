#!/bin/bash

cpus=$(nproc)
arch=`dpkg --print-architecture`
echo "Architecture is $arch"

hostname=`awk '{print $1}' /etc/hostname`
cpus=$(nproc)

#OpenMP env vars
#export OMP_BLOCKTIME=0
export OMP_PLACES=cores
export OMP_PROC_BIND=close
#export OMP_NUM_THREADS=$(($cpus/2))

runtime=xq-ws #xq #omp
test_type=(taskbench)
test_args=(0.001 0.01 0.1 1 10 100 1000) #microseconds
num_threads=(96 192 384) #$(($cpus/2))

#echo "Performing tests with $OMP_NUM_THREADS..."

for type in "${test_type[@]}"
do
  summaryfilename=${hostname}-${runtime}-${type}-time
  for args in "${test_args[@]}"
  do
    for t in "${num_threads[@]}"
    do
      for run in {1..3}
      do
         OMP_NUM_THREADS=$t ./taskbench --delay-time $args 2>&1 | tee -a $summaryfilename
        echo ""
      done
    done
  done
done
