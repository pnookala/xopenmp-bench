CC = clang -g -O3
CFLAGS = -Wall -fPIC -I/usr/local/include -fopenmp
LDFLAGS = -L/usr/local/lib -fopenmp -lm

all: taskbench

taskbench: build/taskbench.o
	$(CC) $^ -o $@ $(LDFLAGS)

build/%.o: %.c build
	$(CC) $(CFLAGS) -c $< -o $@

build:
	mkdir build

clean:
	rm -rf build

